@extends('base')

@section('content')

    <form action="{{ route('password.update') }}" method="post">
        @csrf

        <input type="hidden" name="token" value="{{ $request->token }}">

        <div>
            <label for="email">Email</label>
            <input type="email" id="email" name="email" value="{{ old('email', $request->email) }}"/>
        </div>

        <div>
            <label for="password">Password</label>
            <input type="password" id="password" name="password"/>
        </div>

        <div>
            <label for="password_confirmation">Confirm Password</label>
            <input type="password" id="password_confirmation" name="password_confirmation">
        </div>


        <button type="submit">Reset Password</button>

    </form>

@endsection
