@extends('base')

@section('content')
    <form action="{{ route('password.change') }}" method="post">
        @csrf
        @method('PUT')
        <div class="row g-3 align-items-center">
            <div class="col-auto">
                <label for="inputPassword6" class="col-form-label">Старый пароль</label>
            </div>
            <div class="col-auto">
                <input name="old_password" type="password" id="inputPassword6" class="form-control"
                       aria-labelledby="passwordHelpInline">
            </div>
        </div>
        <div class="row g-3 align-items-center">
            <div class="col-auto">
                <label for="inputPassword6" class="col-form-label">Новый пароль</label>
            </div>
            <div class="col-auto">
                <input name="new_password" type="password" id="inputPassword6" class="form-control"
                       aria-labelledby="passwordHelpInline">
            </div>
        </div>

        <button type="submit">Изменить</button>

    </form>

@endsection
