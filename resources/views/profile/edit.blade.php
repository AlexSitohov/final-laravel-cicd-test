@extends('base')

@section('content')

    <form action="{{ route('profile.update') }}" method="post">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input name="name" type="text" value="{{$user->name}}" class="form-control" id="name">
        </div>
        <div class="mb-3">
            <label for="mail" class="form-label">Email address</label>
            <input name="email" type="email" value="{{$user->email}}" class="form-control" id="mail"
                   placeholder="name@example.com">
        </div>

        <button type="submit">Изменить</button>

    </form>

    <a href="{{route('password.edit')}}">Изменить пароль</a>

@endsection
