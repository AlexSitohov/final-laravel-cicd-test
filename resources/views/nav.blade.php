<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('posts.index') }}">Главная страница</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                @if($user)
                    @can('create posts')
                        <a class="nav-link active" aria-current="page" href="{{ route('posts.create') }}">Создать новый
                            пост</a>
                    @endcan
                    <a class="nav-link active" aria-current="page" href="{{ route('dashBoard.index') }}">Личный
                        кабинет</a>
                    @can('see unconfirmed posts')
                        <a class="nav-link active" aria-current="page" href="{{ route('posts.unconfirmed') }}">Не
                            подтвержденные посты</a>

                    @endcan
                    <a class="nav-link active" aria-current="page" href="/admin">Админка</a>

                    <a class="nav-link active" aria-current="page" href="{{ route('users.index') }}">Пользователи
                        сайта</a>


                    <a class="nav-link active" aria-current="page"
                       href="{{ route('users.show', $user->id) }}">{{$user->name}}</a>

                    <a class="nav-link active" aria-current="page" href="{{ route('auth.logout') }}">Выйти</a>

                @else
                    <a class="nav-link active" aria-current="page" href="{{ route('auth.show-register') }}">Зарегистрироваться</a>
                    <a class="nav-link" href="{{ route('auth.show-login') }}">Войти</a>
                @endif

            </div>
        </div>
    </div>
</nav>

