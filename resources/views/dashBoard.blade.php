@extends('base')

@section('content')
    <a href="{{route('profile.edit')}}">Изменить данные</a>
    <br>
    Вы оставляли коментарии в постах: <br>
    @foreach($postsWithComment as $post)

        <a href="{{route('posts.show', $post->id)}}">{{ $post->title }}</a>        <br>

    @endforeach
    <br>
    Вы оценили посты: <br>
    @foreach($postsWithUserLike as $post)

        <a href="{{route('posts.show', $post->id)}}">{{ $post->title }}</a>        <br>

    @endforeach

@endsection
