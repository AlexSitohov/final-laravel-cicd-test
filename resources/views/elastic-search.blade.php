@extends('base')

@section('content')
    <div class="filter">
        <form method="GET" action="{{ route('elastic-search') }}">
            <div>
                <label for="search_field">Поиск...</label>
                <input name="search_field" @if(isset($_GET['search_field'])) value="{{$_GET['search_field']}}"
                       @endif type="text" id="search_field">
            </div>

            <button type="submit">Применить</button>
        </form>
    </div>


    <div class="posts">

        @foreach($posts as $post)

            <div class="post">
                <a href="{{route('posts.show', $post->id)}}" class="post-title">{{ $post->title }}</a>
                <p class="post-body">{{ $post->body }}</p>
                <p>{{ $post->category->title }}</p>
            </div>

        @endforeach

    </div>

    <div class="pagination">
{{--        {{ $posts->withQueryString()->links() }}--}}
    </div>

@endsection
