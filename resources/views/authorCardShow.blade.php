@extends('base')

@section('content')

{{--    {{$card->user->name}} <br><br>--}}

    @if($bal) bal:  {{$bal}} @endif

    <div>
        <form action="{{route('authorCardComment', $card->id)}}" method="POST">
            @csrf
            <input id="body" name="body" type="text">
            <button type="submit">Коментировать</button>
        </form>
    </div>

    <div>
        <form action="{{route('authorCardRate', $card->id)}}" method="POST">
            @csrf
            <label for="bal">Оцените карточку автора:</label>
            <select name="bal" id="bal">
                <option value="1">1 балл</option>
                <option value="2">2 балла</option>
                <option value="3">3 балла</option>
                <option value="4">4 балла</option>
                <option value="5">5 баллов</option>
            </select>

            <button type="submit">Поставить оценку</button>
        </form>
    </div>

    <br>

@php($comments = $card->comments()->where('status', true)->get())
    @foreach($comments as $comment)
            {{$comment->user->name}}: id: {{$comment->id}} {{$comment->body}}
        <br>
    @endforeach
    <br>

    @foreach($card->user->posts as $post)
        <a href="{{route('posts.show', $post->id)}}">{{$post->title}}</a>
        <br>
       {{$post->user->name}} <br><br>
    @endforeach






@endsection
