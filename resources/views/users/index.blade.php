@extends('base')

@section('content')

    @if(Auth::user()->hasRole('admin'))
    <div class="filter">
        <form method="GET" action="{{ route('users.index') }}">
            <label for="role">Роль:</label>
            <select name="role" id="role">
                <option value="all">Все</option>
                @foreach($roles as $role)
                    <option value="{{$role}}">{{$role}}</option>
                @endforeach
            </select>
            <button type="submit">Применить</button>
        </form>
    </div>
    @endif






    <div class="posts">

        @foreach($users as $user)

            <div class="post">
                <div>
                    @if($user->getMedia('Avatars')->last())
                        <img src="{{$user->getMedia('Avatars')->last()->getUrl()}}" alt=""
                             style="width: 20%;height: 20%;">
                    @endif
                </div>
                @if($user->hasRole('author'))
                    Автор
                @endif
                <a href="{{route('users.show', $user->id)}}" class="post-title">{{ $user->name }}</a>
            </div>

        @endforeach

    </div>

@endsection

