@extends('base')

@section('content')
    @if($user->hasRole('author'))
        <a href="{{route('authorCardShow', $user->id)}}">Карточка автора</a>
    @endif

    {{$user->name}}
    @if($user->hasRole('author'))
        - Автор
    @endif


    @if($user->hasRole('author') && Auth::user()->hasRole('admin'))
        <form action="{{route('giveAuthorRole', $user->id)}}" method="POST">
            @csrf
            <button type="submit">Понизить^^</button>
        </form>
    @endif



    @if(!$user->hasRole('author')  && Auth::user()->hasRole('admin'))
        <form action="{{route('giveAuthorRole', $user->id)}}" method="POST">
            @csrf
            <button type="submit">Назначить Автором</button>
        </form>
    @endif
    <br>
    @if($user->hasRole('author'))
        Аватары автора {{$user->name}}
        <div>
            @foreach( $user->getMedia('Avatars') as $picture)
                <img src="{{$picture->getUrl()}}" alt="" style="width: 20%;height: 20%;">
            @endforeach
        </div>
    @endif

    @if($user->id === Auth::user()->id && $user->hasRole('author'))
        <form action="{{route('setAvatar')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="file" name="file">
            <button type="submit">Загрузить аватар</button>
        </form>

    @endif
@endsection

