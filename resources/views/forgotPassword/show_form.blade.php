@extends('base')

@section('content')
<form action="{{ route('password.request') }}" method="POST">
    @csrf
    <div class="mb-3">
        <label for="email" class="form-label">Email address</label>
        <input name="email" type="email" class="form-control" id="email" placeholder="name@example.com">
    </div>

    <button type="submit">Отправить</button>

</form>
@endsection
