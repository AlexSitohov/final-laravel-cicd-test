@extends('base')

@section('content')

    <form action="{{ route('posts.store') }}" method="post">
        @csrf
        <div>
            <label for="title">title</label>
            <input name="title" type="text" id="title" value="{{old('title')}}">
        </div>
        <div>
            <label for="body">text</label>
            <textarea name="body" id="body" cols="30" rows="10">{{old('body')}}</textarea>
        </div>
        <div>
            <label for="category_id" class="form-label">category</label>
            <select name="category_id" id="category_id" class="form-select form-select-sm"
                    aria-label=".form-select-sm example">
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : '' }}>{{ $category->title }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit">Создать</button>
    </form>


    @role('admin|manager')
    <script src="https://cdn.tiny.cloud/1/faep71gvfk9mdj8p976ymv4uirph0pu44hlwud8239540f30/tinymce/6/tinymce.min.js"
            referrerpolicy="origin"></script>


    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss',
            toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
            mergetags_list: [
                {value: 'First.Name', title: 'First Name'},
                {value: 'Email', title: 'Email'},
            ],
        });
    </script>
    @endrole

@endsection
