@extends('base')

@section('content')
    <div class="filter">
        <form method="GET" action="{{ route('posts.index') }}">
            <div>
                <label for="search_field">Поиск...</label>
                <input class="form-control" name="search_field" @if(isset($_GET['search_field'])) value="{{$_GET['search_field']}}"
                       @endif type="text" id="search_field">
            </div>
            <div>
                <label for="category_search">Категория:</label>
                <select name="category_search" id="category_search">
                    <option></option>
                    @foreach($categories as $category)
                        <option id="category_search" value="{{$category->id}}" @if(isset($_GET['category_search'])) @if($_GET['category_search'] == $category->id) selected @endif @endif>{{$category->title}} {{$category->id}}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit">Применить</button>
        </form>
    </div>


    <div class="posts">

        @foreach($posts as $post)

            <div class="post">
                <a href="{{route('posts.show', $post->id)}}" class="post-title">{{ $post->title }}</a>
                <p class="post-body">{{ $post->body }}</p>
{{--                <p>{{ $post->category->title }}</p>--}}
                <p>{{ $post->category->title }}</p>
            </div>

        @endforeach

    </div>

    <div class="pagination">
        {{ $posts->withQueryString()->links() }}
    </div>

@endsection
