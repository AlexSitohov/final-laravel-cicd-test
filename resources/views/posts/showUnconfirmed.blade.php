@extends('base')

@section('content')

    {{$post->id}}<br>
    {{$post->title}}<br>
    {!! $post->body !!}<br>
    {{$post->category->title}}<br>
    {{$post->user->name}}<br>

    @can('confirm unconfirmed posts')
        <form action="{{ route('posts.confirm-unconfirmed', $post->id) }}" method="POST">
            @csrf
            <button type="submit">Confirm</button>
        </form>
    @endcan

@endsection
