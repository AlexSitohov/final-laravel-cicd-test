<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <title>{{$post->title}}</title>
</head>
<body>
@include('nav')


{{$post->id}}<br>
{{$post->title}}<br>
{!! $post->body !!}<br>
{{$post->category->title}}<br>
{{$post->user->name}}<br>


@if(Auth::user())
    @if($canComment)

        <form action="{{route('comment.store', $post->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <input id="body" name="body" type="text">
            <label for="file">media</label>
            <input type="file" name="file" id="file">
            <button type="submit">Коментировать</button>
        </form>
        <br>
    @endif
@endif

@if(Auth::user())
    <form action="{{route('likes.store', $post->id)}}" method="POST">
        @csrf
        <button type="submit">Like</button>
    </form>

@endif


<br>
Лайки:
{{$post->likes->count()}}
<br><br>

Коментарии: <br>

@foreach($post->comments as $comment)
    {{$comment->user->name}}: id: {{$comment->id}} {{$comment->body}}
    @can('delete posts')
        <a href="{{route('comment.delete', $comment->id)}}">удалить</a>
    @endcan
    @foreach($comment->getMedia('commentsMedia') as $picture)
        <img src="{{$picture->getUrl()}}" alt="" style="width: 5%;height: 5%;">
    @endforeach
    <br>
    @foreach($comment->comments as $subcomment)
        -----------{{$subcomment->user->name}}: id: {{$subcomment->id}} {{$subcomment->body}}
        @can('delete posts')
            <a href="{{route('comment.delete', $comment->id)}}">удалить</a>
        @endcan
        @foreach($subcomment->getMedia('commentsMedia') as $picture)
            <img src="{{$picture->getUrl()}}" alt="" style="width: 5%;height: 5%;"><br>
        @endforeach<br>
    @endforeach
@endforeach

поделиться
<ul>
    <li>
        <a href="https://vk.com/share.php?url={{route('posts.show',$post->id)}}" target="_blank">vk</a>
    </li>
    <li>
        <a href="https://telegram.me/share/url?url={{route('posts.show',$post->id)}}&text={{route('posts.show',$post->id)}}"
           target="_blank">tg</a>
    </li>
</ul>


</body>
</html>

