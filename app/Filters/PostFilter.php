<?php

namespace App\Filters;

use App\Models\Post;

class PostFilter extends QueryFilter {

    public function category_search($id = null)
    {
        return $this->builder->when($id, function ($query) use ($id) {
            $query->where('category_id', $id);
        });
    }

//    public function search_field(string $search_string = '')
//    {
//        return $this->builder->where('title', 'like', "%$search_string%")
//            ->orWhere('body', 'like', "%$search_string%");
//    }


}
