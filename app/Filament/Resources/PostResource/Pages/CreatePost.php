<?php

namespace App\Filament\Resources\PostResource\Pages;

use App\Filament\Resources\PostResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;
use Filament\Notifications\Actions\Action;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Notification;
use App\Notifications\PostNotification;
use App\Notifications\FromAdminPostNotification;
use Illuminate\Support\Facades\Auth;



class CreatePost extends CreateRecord {

    protected static string $resource = PostResource::class;

    protected function mutateFormDataBeforeCreate(array $data): array
    {
        $data['user_id'] = auth()->id();

        return $data;
    }

    protected function getRedirectUrl(): string
    {
        return $this->getResource()::getUrl('index');
    }
//    public static function getUserIdFormField(): Forms\Components\TextInput
//    {
//        return TextInput::make('user_id');
////            ->required()
////            ->reactive()
////            ->afterStateUpdated(fn ($state, callable $set) => $set('slug', Str::slug($state)));
//    }

    protected function afterCreate(): void
    {

        $user = Auth::user();
        // Runs after the form fields are saved to the database.
        $admins = Role::where('name', 'admin')->first()->users;

        $current_user = Auth::user();

        if (!$current_user->hasRole('admin')) {
            $admins->push($current_user);
        }
        Notification::send($admins, new FromAdminPostNotification($user));
    }






}
