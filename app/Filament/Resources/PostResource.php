<?php

namespace App\Filament\Resources;

use App\Filament\Resources\PostResource\Pages;
use App\Filament\Resources\PostResource\RelationManagers;
use App\Models\Post;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Actions\DeleteAction;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\Card;
use Illuminate\Support\Facades\Auth;


class PostResource extends Resource {

    protected static ?string $model = Post::class;

    protected static ?int $navigationSort = 4;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form->schema([Card::make()->schema([Forms\Components\TextInput::make('title')->required()->maxLength(50), //            Forms\Components\TextInput::make('user_id')->required(),
            Select::make('category_id')->relationship('category', 'title')->required(), Textarea::make('body')->required()->maxLength(65535), Forms\Components\Toggle::make('status')->required(),])]);
    }

    public static function table(Table $table): Table
    {
        return $table->columns([Tables\Columns\TextColumn::make('id')->sortable()->searchable(), Tables\Columns\TextColumn::make('title'), Tables\Columns\TextColumn::make('category.title'), Tables\Columns\TextColumn::make('user_id'), Tables\Columns\TextColumn::make('body')->limit(20), Tables\Columns\TextColumn::make('created_at')->dateTime(), Tables\Columns\TextColumn::make('updated_at')->dateTime(), Tables\Columns\IconColumn::make('status')->boolean(),])->filters([//
            ])->actions([EditAction::make(), DeleteAction::make(),])->bulkActions([Tables\Actions\DeleteBulkAction::make(),]);
    }

    public static function getRelations(): array
    {
        return [//
        ];
    }

    public static function getPages(): array
    {
        return ['index' => Pages\ListPosts::route('/'), 'create' => Pages\CreatePost::route('/create'), 'edit' => Pages\EditPost::route('/{record}/edit'),];
    }


    public static function getEloquentQuery(): Builder
    {
        $user = Auth::user();
        if ($user->hasRole(['admin','manager'])) {
            return parent::getEloquentQuery();
        }
        if ($user->hasRole('author')) {
            return parent::getEloquentQuery()->where('user_id', $user->id);
        }

    }
    protected static function getNavigationBadge(): ?string
    {
        $user = Auth::user();
        if ($user->hasRole(['admin','manager'])) {
            return static::$model::count();
        }
        if ($user->hasRole('author')) {
            return static::$model::where('user_id', $user->id)->count();
        }

    }


}
