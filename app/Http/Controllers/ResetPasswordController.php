<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller {

    public function create(Request $request)
    {
        return view('reset-password', ['request' => $request]);
    }

    public function store(ResetPasswordRequest $request)
    {
        if (($request->get('password') !== $request->get('password_confirmation'))) return response('not correct');
        $status = Password::reset($request->only('email', 'password', 'password_confirmation', 'token'), function ($user) use ($request) {
            $user->forceFill(['password' => Hash::make($request->password), 'remember_token' => Str::random(60)])->save();
        });

        if ($status === Password::PASSWORD_RESET) {
            return redirect()->route('auth.login')->with('status', trans($status));
        }

        return back()->withInput($request->only('email'));
    }
}
