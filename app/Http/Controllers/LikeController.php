<?php

namespace App\Http\Controllers;

use App\Models\Like;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller {

    public function store(Post $post)
    {
        $user_id = Auth::user()->id;
        $like_query = Like::where(['user_id' => $user_id, 'post_id' => $post->id]);
        $like = $like_query->first();
        if ($like) {
            $like_query->delete();
            return redirect()->route('posts.show', $post->id);
        } else {
            $like = new Like;
            $like->user_id = $user_id;
            $like->post_id = $post->id;
            $like->save();
            return redirect()->route('posts.show', $post->id);
        }

    }
}
