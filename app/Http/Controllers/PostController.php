<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Filters\PostFilter;
use App\Notifications\PostNotification;
use Illuminate\Support\Facades\Notification;
use Filament\Notifications\Notification as NF;
use App\Models\User;
use Spatie\Permission\Models\Role;


class PostController extends Controller {

    public function index(PostFilter $filter, Request $request)
    {

        $q = $request->get('search_field');
        if ($q) {
            $posts = Post::search($q)->where('status', true)->when(request('category_search'), function ($query, $categoryId) {
                return $query->where('category_id', $categoryId);
            })->paginate(12);
        } else {
            $posts = Post::where('status', true)->when(request('category_search'), function ($query, $categoryId) {
                return $query->where('category_id', $categoryId);
            })->paginate(12);
        }


        $categories = Category::all();
        return view('posts.index', compact('posts', 'categories'));
    }

    public function show(Post $post)
    {
        $currentUser = Auth::user();
        if ($currentUser) {
            $flag = $currentUser->hasRole('author');
            if (($flag && $post->user_id === $currentUser->id) || !$flag) {
                $canComment = true;
            } else {
                $canComment = false;
            }
            return view('posts.show', compact('post'), compact('canComment'));

        } else {
            return view('posts.show', compact('post'));
        }


    }

    public function create()
    {
        $categories = Category::all();
        return view('posts.create', compact('categories'));
    }

    public function store(StorePostRequest $request)
    {
        $data = $request->validated();

        $data['user_id'] = Auth::user()->id;

        $post = Post::create($data);

        $admins = Role::where('name', 'admin')->first()->users;

        $current_user = Auth::user();

        if(!$current_user->hasRole('admin')){
            $admins->push($current_user);
        }
        Notification::send($admins, new PostNotification($post));
        return redirect()->route('posts.index');
    }

//    Unconfirmed posts
    public function indexUnconfirmed()
    {
        $posts = Post::where('status', false)->get();
        return view('posts.unconfirmed', compact('posts'));
    }

    public function showUnconfirmed(Post $post)
    {
        return view('posts.showUnconfirmed', compact('post'));
    }

    public function confirmUnconfirmed(Post $post)
    {
        $post->status = true;
        $post->save();
        return redirect()->route('posts.index');
    }


}
