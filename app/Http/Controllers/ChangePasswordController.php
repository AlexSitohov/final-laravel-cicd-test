<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Services\ChangePasswordService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller {

    public $service;

    public function __construct(ChangePasswordService $service)
    {
        $this->service = $service;
    }

    public function edit()
    {

        return view('profile.changePassword');
    }

    public function change(ChangePasswordRequest $request)
    {
        return $this->service->update($request);
    }
}
