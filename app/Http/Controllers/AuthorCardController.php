<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\AuthorCard;
use App\Http\Requests\AuthorCardRequest;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;
use App\Models\CardRaiting;
use Illuminate\Support\Facades\DB;

class AuthorCardController extends Controller {

    public function show(User $user)
    {
        $card = $user->authorCard;

        $bal = 0;

        if ($card === null) {
            $card = new AuthorCard;

            $card->user_id = $user->id;
            $card->save();

        }

        if ($card->cardRating) {
            foreach ($card->cardRating as $raiting) {
                $bal += $raiting->bal;

            }
            if(count($card->cardRating) > 0) {
                $bal = $bal/count($card->cardRating);
            }

        }
        return view('authorCardShow', compact('card'), compact('bal'));
    }

    public function comment(AuthorCardRequest $request, $authorCardId)
    {
        $data['body'] = $request->get('body');
        $data['comment_id'] = $authorCardId;
        $data['comment_type'] = AuthorCard::class;
        $data['user_id'] = Auth::user()->id;
        $data['status'] = false;

        Comment::create($data);

        return redirect()->route('authorCardShow', $authorCardId);
    }

    public function rate(Request $request, $authorCardId)
    {
        $card = AuthorCard::find($authorCardId);
        $user = $card->user;

        $user_id = Auth::user()->id;
        $rate_query = CardRaiting::where(['user_id' => $user_id, 'author_card_id' => $authorCardId]);
        $rate = $rate_query->first();
        if ($rate) {
            DB::table('cards_ratings')
                ->where(['user_id' => $user_id, 'author_card_id' => $authorCardId])
                ->update(['bal' => $request->get('bal')]);
            return redirect()->route('authorCardShow', $user->id);
        } else {
            $rate = new CardRaiting;
            $rate->user_id = $user_id;
            $rate->author_card_id = $authorCardId;
            $rate->bal = $request->get('bal');
            $rate->save();

            return redirect()->route('authorCardShow', $user->id);
        }
    }
}
