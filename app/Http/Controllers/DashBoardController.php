<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashBoardController extends Controller {

    public function index()
    {
        $user = Auth::user();
        $postsWithComment = Post::whereHas('comments', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        })->get();
        $postsWithUserLike = Post::whereHas('likes', function ($query) use ($user) {
            $query->where('user_id', $user->id);
        })->get();
        return view('dashBoard', ['postsWithComment' => $postsWithComment, 'postsWithUserLike' => $postsWithUserLike]);
    }
}
