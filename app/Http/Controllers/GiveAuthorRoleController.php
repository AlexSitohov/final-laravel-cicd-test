<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class GiveAuthorRoleController extends Controller {

    public function giveAuthorRole(User $user)
    {
        if ($user->hasRole('author')) {
            $user->removeRole('author');
        } else {
            $user->assignRole('author');

        }
        return redirect()->route('users.show', $user->id);
    }
}
