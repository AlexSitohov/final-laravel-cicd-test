<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Services\CommentService;
use Illuminate\Http\Request;

class CommentController extends Controller {

    public $service;

    public function __construct(CommentService $service)
    {
        $this->service = $service;
    }

    public function store(Request $data, Post $post)
    {
        $this->service->store($data, $post);
        return redirect()->route('posts.show', $post->id);
    }

    public function delete(Comment $comment)
    {
        foreach ($comment->comments as $subcomment) $subcomment->delete();
        $comment->delete();
        return redirect()->route('posts.index');
    }
}


