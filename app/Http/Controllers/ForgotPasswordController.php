<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Jobs\ForgotPasswordJob;

class ForgotPasswordController extends Controller {

    public function create()
    {
        return view('forgotPassword.show_form');
    }

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ]);

        $email = $request->email;

        dispatch(new ForgotPasswordJob($email));

        return back()->with('success', 'Ссылка для сброса пароля отправлена на ваш email.');
    }
}
