<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller {

    public function index(Request $request)
    {

        $users = User::all();
        if ($request->has('role')) {
            if ($request->input('role') === 'all') {
                // Если категория выбрана как "все", то ничего не делаем
            } else {
                $users = User::role($request->input('role'))->get();
            }
        }


        $roles = DB::table('roles')->pluck('name');

        return view('users.index', compact('users', 'roles'));
    }

    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    public function setAvatar(Request $request)
    {
        $user = Auth::user();

        if ($user->hasRole('author')) {
            if ($request->hasFile('file')) {

                $user->addMedia($request->file('file'))->toMediaCollection('Avatars');

                return redirect()->route('users.show', $user->id);

            }
        } else {
            return response('вы не автор!!!');

        }
    }
}
