<?php

namespace App\Services;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordService
{
    public function update($request)
    {
        $user = Auth::user();
        $old_password = $request->get('old_password');
        $new_password = $request->get('new_password');

        if (Auth::attempt(['email' => $user->email, 'password' => $old_password])) {
            $user->password = Hash::make($new_password);
            $user->save();
            return redirect()->route('dashBoard.index');
        } else {
            return redirect()->route('password.edit');
        }
    }

}
