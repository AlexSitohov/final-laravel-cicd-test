<?php

namespace App\Services;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;

class CommentService {

    public function store($data, $post)
    {
        $currentUser = Auth::user();
        $flag = $currentUser->hasRole('author');
        if (($flag && $post->user_id === $currentUser->id) || !$flag) {
            if (preg_match('/^@(\d+)\s*/', $data->get('body'), $matches)) {
                $comment_id = $matches[1];
                $data['comment_id'] = $comment_id;
                $data['body'] = preg_replace('/^@\d+\s*/', '', $data['body']);
                $data['comment_type'] = Comment::class;
            } else {
                $data['comment_id'] = $post->id;
                $data['comment_type'] = Post::class;
            }
            $data['user_id'] = Auth::user()->id;
            if ($data->hasFile('file')) {
                $file = $data['file'];
                $data = $data->toArray();
                unset($data['file']);
                $comment = Comment::create($data);
                $comment->addMedia($file)->toMediaCollection('commentsMedia');
            } else {
                $data = $data->toArray();
                unset($data['file']);
                Comment::create($data);

            }

        }


    }
}
