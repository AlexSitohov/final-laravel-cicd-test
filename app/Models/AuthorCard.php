<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuthorCard extends Model {

    use HasFactory;

    protected $guarded = [];

    protected $table = 'author_cards';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'comment');
    }

    public function cardRating()
    {
        return $this->hasMany(CardRaiting::class);
    }




}
