<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';

    protected $guarded = false;


    function author()
    {
        return $this->belongsTo(User::class);

    }

    function posts()
    {
        return $this->hasMany(Post::class);
    }
}
