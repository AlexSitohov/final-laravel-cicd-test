<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AdminUserSeeder extends Seeder {

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin_user = User::create(['name' => 'admin', 'email' => 'admin@gmail.com', 'confirmed' => true, 'password' => Hash::make('123')]);

        $role = Role::create(['name' => 'admin']);

        Permission::create(['name' => 'see logs']);
        Permission::create(['name' => 'see unconfirmed posts']);
        Permission::create(['name' => 'confirm unconfirmed posts']);
        Permission::create(['name' => 'delete posts']);


        $role->syncPermissions(['delete posts', 'see logs', 'see unconfirmed posts', 'confirm unconfirmed posts']);

        $admin_user->assignRole('admin');
    }

}
