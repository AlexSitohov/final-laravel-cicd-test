<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class ManagerUserSeeder extends Seeder {

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $manager_user = User::create(['name' => 'manager', 'email' => 'manager@gmail.com', 'confirmed' => true, 'password' => Hash::make('123')]);

        $role = Role::create(['name' => 'manager',]);

        $role->syncPermissions(['delete posts', 'see unconfirmed posts']);

        $manager_user->assignRole('manager');
    }
}
