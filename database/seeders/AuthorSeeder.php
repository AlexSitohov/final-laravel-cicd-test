<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AuthorSeeder extends Seeder {

    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $role = Role::create(['name' => 'author']);

        Permission::create(['name' => 'create posts']);
        Permission::create(['name' => 'update posts']);


        $role->syncPermissions(['create posts', 'update posts', 'delete posts']);

    }
}
