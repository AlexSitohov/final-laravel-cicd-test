<?php

use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\DashBoardController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ResetPasswordController;
use App\Http\Controllers\Web\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\GiveAuthorRoleController;
use App\Http\Controllers\AuthorCardController;
use App\Http\Controllers\ElasticSearchController;


Route::get('/', [PostController::class, 'index'])->name('posts.index');
Route::get('/posts/create', [PostController::class, 'create'])->name('posts.create')->middleware('can:create posts');
Route::post('/posts', [PostController::class, 'store'])->name('posts.store')->middleware('can:create posts');;
Route::get('/posts/show/{post}', [PostController::class, 'show'])->name('posts.show');

Route::get('/users', [UsersController::class, 'index'])->name('users.index');
Route::get('/users/{user}', [UsersController::class, 'show'])->name('users.show');


Route::get('/posts/unconfirmed', [PostController::class, 'indexUnconfirmed'])->name('posts.unconfirmed')->middleware('can:see unconfirmed posts');
Route::get('/posts/unconfirmed/{post}', [PostController::class, 'showUnconfirmed'])->name('posts.show-unconfirmed')->middleware('can:see unconfirmed posts');
Route::post('/posts/unconfirmed/{post}', [PostController::class, 'confirmUnconfirmed'])->name('posts.confirm-unconfirmed')->middleware('can:confirm unconfirmed posts');


Route::prefix('auth')->group(function () {
    Route::get('/register', [AuthController::class, 'showRegister'])->name('auth.show-register');
    Route::post('/register', [AuthController::class, 'register'])->name('auth.register');

    Route::get('/login', [AuthController::class, 'showLogin'])->name('auth.show-login');
    Route::post('/login', [AuthController::class, 'login'])->name('auth.login');

    Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');
});

Route::middleware('auth')->group(function () {
    Route::get('/dashboard', [DashBoardController::class, 'index'])->name('dashBoard.index');
    Route::post('/comments/posts/{post}', [CommentController::class, 'store'])->name('comment.store');
    Route::post('/likes/posts/{post}', [LikeController::class, 'store'])->name('likes.store');
});


Route::get('/comments/delete/{comment}', [CommentController::class, 'delete'])->name('comment.delete')->middleware('auth');;


Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit')->middleware('auth');;

Route::put('/profile', [ProfileController::class, 'update'])->name('profile.update')->middleware('auth');;

Route::get('/password', [ChangePasswordController::class, 'edit'])->name('password.edit')->middleware('auth');;

Route::put('/password', [ChangePasswordController::class, 'change'])->name('password.change')->middleware('auth');;


Route::get('/forgot-password', [ForgotPasswordController::class, 'create'])->name('password.request');

Route::post('/forgot-password', [ForgotPasswordController::class, 'store'])->name('password.email');

Route::get('/reset-password', [ResetPasswordController::class, 'create'])->name('password.reset');

Route::post('/reset-password', [ResetPasswordController::class, 'store'])->name('password.update');


Route::post('/give-author-role/{user}', [GiveAuthorRoleController::class, 'giveAuthorRole'])->name('giveAuthorRole');

Route::post('/set-avatar', [UsersController::class, 'setAvatar'])->name('setAvatar')->middleware('auth');;

Route::get('/show/author-card/{user}', [AuthorCardController::class, 'show'])->name('authorCardShow');

Route::post('/comment/author-card/{authorCard}', [AuthorCardController::class, 'comment'])->name('authorCardComment')->middleware('auth');

Route::post('/rate/author-card/{authorCard}', [AuthorCardController::class, 'rate'])->name('authorCardRate')->middleware('auth');

Route::get('/elastic-search', [ElasticSearchController::class, 'index'])->name('elastic-search');
Route::get('/test_data', [ElasticSearchController::class, 'test_data'])->name('test_data');

