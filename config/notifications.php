<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Dark mode
    |--------------------------------------------------------------------------
    |
    | By enabling this setting, your notifications will be ready for Tailwind's
    | Dark Mode feature.
    |
    | https://tailwindcss.com/docs/dark-mode
    |
    */

    'dark_mode' => true,

    /*
    |--------------------------------------------------------------------------
    | Database notifications
    |--------------------------------------------------------------------------
    |
    | By enabling this feature, your users are able to open a slide-over within
    | the app to view their database notifications.
    |
    */

    'database' => [
        'enabled' => true,
        'polling_interval' => null,
        // ...
    ],

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | This is the configuration for the general layout of notifications.
    |
    */

    'layout' => [
        'max_content_width' => null,
        'forms' => [
            'actions' => [
                'alignment' => 'left',
            ],
        ],
        'sidebar' => [
            'is_collapsible_on_desktop' => false,
            'groups' => [
                'are_collapsible' => true,
            ],
            'width' => null,
            'collapsed_width' => null,
        ],

    ],

];
